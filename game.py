import pygame
import numpy as np
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
from PIL import Image

from main import is_minus, classify_digit

RES = 8
DIMS = (28*5, 28 + 28//2)
SCREEN = (RES * DIMS[0], RES * DIMS[1])
display = pygame.display.set_mode(SCREEN)
coords = []
np_surface = np.zeros(DIMS[::-1])
print(len(np_surface))


def split_number_image(image, labels, coordinates):
    symbols = []
    for label in np.unique(labels):
        if label == -1:
            continue  # Skip points not belonging to clusters
        indices = np.where(labels == label)
        x_values, y_values = coordinates[indices].T
        min_x, max_x = max(0, np.min(x_values)-4), np.max(x_values) + 4
        min_y, max_y = max(0, np.min(y_values)-4), np.max(y_values) + 4
        symbol_image = image[min_x:max_x, min_y:max_y]
        symbols.append([min_y, symbol_image])
    symbols = list(map(lambda x: x[1], sorted(symbols, key=lambda x: x[0])))
    return symbols



class Tile:
    def __init__(self, c, r):
        self.c = c
        self.r = r
        self.x = self.c * RES
        self.y = self.r * RES
        self.state = 0
        self.rect = pygame.Rect(self.x, self.y, RES, RES)

    def draw(self):
        pygame.draw.rect(display, self.getColor(), self.rect)

    def getColor(self):
        if self.state == 0:
            return 'black'
        return 'white'


grid = []
for r in range(DIMS[1]):
    row = []
    for c in range(DIMS[0]):
        tile = Tile(c, r)
        row.append(tile)
    grid.append(row)


def reset():
    for row in grid:
        for tile in row:
            tile.state = 0
            global coords
            global np_surface
            coords = []
            np_surface = np.zeros((28 * 2, 28 * 5))

def draw():
    display.fill("red")
    for row in grid:
        for tile in row:
            tile.draw()
    pygame.display.flip()

is_mouse_down = False
def update():
    global is_mouse_down
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            is_mouse_down = True
        elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            is_mouse_down = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_BACKSPACE:
                reset()
            elif event.key == pygame.K_SPACE:
                coordinates = np.column_stack(np.where(np_surface > 0))
                if coordinates.size > 0:
                    # Apply the DBSCAN algorithm
                    dbscan = DBSCAN(eps=0.5, min_samples=5)
                    labels = dbscan.fit_predict(StandardScaler().fit_transform(coordinates))
                    symbols = split_number_image(np_surface, labels, coordinates)
                    result = ""
                    if len(symbols) > 0:
                        first_token = symbols[0]
                        image_pil = Image.fromarray(first_token*255)
                        image_pil = image_pil.resize((28, 28))
                        is_neg = is_minus(image_pil)
                        if is_neg:
                            result += "-"
                            symbols.pop(0)

                    print(len(symbols))
                    for symbol in symbols:
                        image_pil = Image.fromarray(symbol*255)
                        image_pil = image_pil.resize((28, 28))
                        image_pil.show("asd")
                        digit = classify_digit(image_pil)
                        result += digit
                    print()
                    print("#"*40 + " Предсказание: " + "#"*40)
                    print(result)
                    print("#"*95)

        if is_mouse_down:
            for row in grid:
                for tile in row:
                    if tile.rect.collidepoint(pygame.mouse.get_pos()):
                        tile.state = 1
                        coords.append([tile.c, tile.r])
                        np_surface[tile.r, tile.c] = 1.0


if __name__ == '__main__':
    while True:
        draw()
        update()

