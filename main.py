import torch
import torchvision.transforms as tfs
from torch import nn
from torch.nn import functional as F


print("Imported Pytorch and stuff")

# Define the transformation (use the same transformations as during training)
transform = tfs.Compose([
    tfs.Resize((28, 28)),
    tfs.Grayscale(num_output_channels=1),  # If needed for your model
    tfs.ToTensor(),
    tfs.Normalize((0.5,), (0.5,)),  # If needed for your model
])


class LeNet(nn.Module):
    def __init__(self):
        super(LeNet, self).__init__()
        self.conv1 = nn.Conv2d(1, 6, 3)  # 28x28x1 -> 26x26x6
        self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)  # 26x26x6 -> 13x13x6
        self.conv2 = nn.Conv2d(6, 16, 3)  # 13x13x6 -> 11x11x16
        self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)  # 11x11x16 -> 5x5x16
        self.flatten = nn.Flatten()
        self.fc1 = nn.Linear(5 * 5 * 16, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = self.pool1(x)
        x = F.relu(self.conv2(x))
        x = self.pool2(x)
        x = self.flatten(x)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


class LeNet2(LeNet):
    def __init__(self):
        super(LeNet2, self).__init__()
        # 1 input image channel, 6 output channels, 3x3 square conv kernel
        # self.conv1 = nn.Conv2d(1, 6, 3)
        # self.pool1 = nn.MaxPool2d(kernel_size=2, stride=2)
        # self.conv2 = nn.Conv2d(6, 16, 3)
        # self.pool2 = nn.MaxPool2d(kernel_size=2, stride=2)
        # self.flatten = nn.Flatten()
        # self.fc1 = nn.Linear(5 * 5 * 16, 120)
        # self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 12)
        self.fc4 = nn.Linear(12, 2)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = self.pool1(x)
        x = F.relu(self.conv2(x))
        x = self.pool2(x)
        x = self.flatten(x)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = self.fc4(x)

        return x


PATH1 = './LenetMNISTModel.pt'
PATH2 = './LenetMinusRecognizerModel.pt'
device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

digits_model = LeNet()
digits_model.load_state_dict(torch.load(PATH1, map_location=device))
digits_model.eval()

minus_model = LeNet2()
minus_model.load_state_dict(torch.load(PATH2, map_location=device))
minus_model.eval()

print("Loaded models")

def is_minus(img):
    input_image = transform(img)

    # Add an extra dimension to simulate a batch of size 1
    input_image = input_image.unsqueeze(0)
    with torch.no_grad():
        output = minus_model(input_image)
        predicted_class = output.argmax(-1).item()
        return predicted_class == 0


def classify_digit(img):
    input_image = transform(img)

    # Add an extra dimension to simulate a batch of size 1
    input_image = input_image.unsqueeze(0)
    with torch.no_grad():
        output = digits_model(input_image)
        print("_"*120)
        print(output.shape, output)
        predicted_class = output.argmax(-1).item()
        return str(predicted_class)




