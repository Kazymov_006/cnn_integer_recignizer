import pygame
import random

RES = 16
DIMS = (28, 28)
SCREEN = (RES * DIMS[0], RES * DIMS[1])
display = pygame.display.set_mode(SCREEN)
LABEL_MINUS = "minus"
LABEL_NOT_MINUS = "not_minus"
PATH = "./minus_dataset"


class Tile:
    def __init__(self, c, r):
        self.c = c
        self.r = r
        self.x = self.c * RES
        self.y = self.r * RES
        self.state = 0
        self.rect = pygame.Rect(self.x, self.y, RES, RES)

    def draw(self):
        pygame.draw.rect(display, self.getColor(), self.rect)

    def getColor(self):
        if self.state == 0:
            return 'black'
        return 'white'


grid = []
for r in range(DIMS[1]):
    row = []
    for c in range(DIMS[0]):
        tile = Tile(c, r)
        row.append(tile)
    grid.append(row)


def reset():
    for row in grid:
        for tile in row:
            tile.state = 0

def draw():
    display.fill("red")
    for row in grid:
        for tile in row:
            tile.draw()
    pygame.display.flip()

is_mouse_down = False
def update():
    global is_mouse_down
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
        elif event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            is_mouse_down = True
        elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            is_mouse_down = False
        elif event.type == pygame.KEYDOWN:
            label = None
            if event.key == pygame.K_SPACE :
                label = LABEL_MINUS
            elif event.key == pygame.K_CAPSLOCK:
                label = LABEL_NOT_MINUS
            if label is not None:
                unique_id = ""
                for _ in range(8):
                    unique_id += str(random.randint(0, 9))
                pygame.image.save(display, f"{PATH}/{label}/{unique_id}.png")
                reset()

        if is_mouse_down:
            for row in grid:
                for tile in row:
                    if tile.rect.collidepoint(pygame.mouse.get_pos()):
                        tile.state = 1


if __name__ == '__main__':
    while True:
        draw()
        update()

