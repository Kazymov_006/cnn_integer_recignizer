import cv2
import os
from os.path import join as jn


PATH = jn(os.getcwd(), "minus_dataset")
DEST_PATH = jn(os.getcwd(),"minus_dataset_resized")

for folder in os.listdir(PATH):
    target_folder = jn(PATH, folder)
    print(f"{target_folder} - {len(os.listdir(target_folder))}")
    for file in os.listdir(target_folder):
        rel_pf = jn(target_folder, file)

        img = cv2.imread(rel_pf, cv2.IMREAD_UNCHANGED)
        res_img = cv2.resize(img, (28, 28))

        new_imname = jn(DEST_PATH, folder, file)

        mirrored = cv2.flip(img, 1)

        new_imname2 = jn(DEST_PATH, folder, "mirrored"+file)

        cv2.imwrite(new_imname, res_img)
        cv2.imwrite(new_imname2, mirrored)
